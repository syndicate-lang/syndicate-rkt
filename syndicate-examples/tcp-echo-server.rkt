#lang syndicate
;;; SPDX-License-Identifier: LGPL-3.0-or-later
;;; SPDX-FileCopyrightText: Copyright © 2021-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

(module+ main
  (require racket/cmdline)
  (require syndicate/drivers/tcp)

  (define host "0.0.0.0")
  (define port 5999)

  (command-line #:once-each
                [("--host" "-H") hostname "Set hostname to listen on"
                 (set! host hostname)]
                [("--port" "-p") port-number "Set port number to listen on"
                 (set! port (string->number port-number))])

  (standard-actor-system (ds)
    (at ds
      (stop-on (asserted (StreamListenerError (TcpLocal host port) $message)))
      (during/spawn (StreamConnection $source $sink (TcpLocal host port))
        (handle-connection source sink
                           #:on-data (lambda (data mode) (send-data sink data mode)))))))
