;;; SPDX-License-Identifier: LGPL-3.0-or-later
;;; SPDX-FileCopyrightText: Copyright © 2018-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

#lang racket/base

(require auxiliary-macro-context)

(define-auxiliary-macro-context
  #:context-name pattern-expander
  #:prop-name prop:pattern-expander
  #:prop-predicate-name pattern-expander?
  #:prop-accessor-name pattern-expander-proc
  #:macro-definer-name define-pattern-expander
  #:introducer-parameter-name current-pattern-expander-introducer
  #:local-introduce-name syntax-local-pattern-expander-introduce
  #:expander-form-predicate-name pattern-expander-form?
  #:expander-transform-name pattern-expander-transform)

(provide (for-syntax
          prop:pattern-expander
          pattern-expander?
          pattern-expander-proc
          syntax-local-pattern-expander-introduce
          pattern-expander-form?
          pattern-expander-transform)
         define-pattern-expander)
