# Third Racket implementation of Syndicate

## Quickstart

    git clone https://git.syndicate-lang.org/syndicate-lang/syndicate-rkt
    cd syndicate-rkt
    make link

For more detail, [see below](#compiling-and-running-the-code).

## The language itself

This repository contains a [Racket](http://racket-lang.org/)
implementation of Syndicate, which includes

 - the implementation of the `#lang syndicate` language, in the
   [`syndicate` directory](syndicate/).

 - a TCP echo server example, which listens for connections on port
   5999 by default, in
   [`syndicate-examples/tcp-echo-server.rkt`](syndicate-examples/tcp-echo-server.rkt).
   Connect to it using, for example, `telnet localhost 5999`.

 - a number of other examples both small and large, in
   [`syndicate-examples/`](syndicate-examples/).

## New design, new implementation

This implementation of Syndicate is based on the *Syndicated Actor
Model*, a design that takes the language-level constructs of facets,
capabilities denoting objects, and dataflow fields to heart. The
implementation integrates these ideas into a facet- and
assertion-oriented actor implementation and a capability-aware
dataspace implementation.

It gains a *significant* performance advantage (10-30x speedup!) over
[2017-era dataspace implementations](https://git.syndicate-lang.org/syndicate-lang/syndicate-2017/src/branch/main/racket)
by representing patterns over assertions in a
[new way](syndicate/HOWITWORKS.md), and a smaller but not
insignificant advantage over
[2019-era implementations](https://git.syndicate-lang.org/syndicate-lang/syndicate-rkt/src/branch/fruit)
by switching to a simpler and more general actor implementation.

The dataspace implementation techniques herein are the subject of a
forthcoming paper. The prototype that embodies the new idea is in
[historical/prototype.rkt](historical/prototype.rkt), and
[syndicate/HOWITWORKS.md](syndicate/HOWITWORKS.md) describes the
approach via prose.

All the drivers end up looking much nicer with this new
implementation. The previously-separate GL-2D support is now
integrated as just another driver (though the timing characteristics
of the old implementation are not precisely preserved). The connection
to the surrounding Racket environment is also much cleaner.

<!-- To see the difference in speed, try out the "many Racket logos" -->
<!-- animation example/demo after installing the `syndicate` and -->
<!-- `syndicate-examples` packages: -->

<!--     racket -l syndicate-examples/gl-2d-many -->

<!-- Hopefully you'll get a smooth 60fps, though I admit I'm running it on -->
<!-- a fairly fast machine so you might need to drop the `sprite-count` in -->
<!-- the code a bit to sustain 60fps. -->

## Compiling and running the code

You will need Racket version 8.1 or later.

Once you have Racket installed, run

<!--     raco pkg install syndicate -->

<!-- to install the package from the Racket package repository, or -->

    raco pkg install --link syndicate

from the root directory of the Git checkout to install the package
from a local snapshot. This will make `#lang syndicate` available to
programs.

Repeat the process with `syndicate-examples` in place of `syndicate`
to install the examples as well.

Alternatively, `make link` from the root directory of the Git checkout
installs both packages from the local copy.

### Running the tests

The implementation test suite lives in [syndicate/test/](syndicate/test/). Run it with:

    raco setup syndicate; raco test -p syndicate

## Distribution

### License

This program is distributed under the terms of the [LGPLv3
license](https://opensource.org/licenses/lgpl-3.0.html) or any later version.

Documentation is distributed under the terms of the [CC BY 4.0
license](https://creativecommons.org/licenses/by/4.0/).

### Authors

 - Tony Garnock-Jones

### Contributors

Contributors can be found by inspecting the history of the source
control repository.

### How to contribute

As contributor, you retain the copyright of your work, but it must be
distributed using the same license as this software. For minor patches
like bug fixes, typo corrections, or improvements to the
documentation, this will be implicit, and will be documented in the
history of the source control repository. In case of major
contributions, you must also update the header of the modified source
code files, specifying something like this:

```
SPDX-License-Identifier: LGPL-3.0-or-later
Copyright (C) 2019-2021 Previous Author <some.email@example.net>
Copyright (C) YYYY-YYYY New Contributor <some.email@example.net>
```
