__ignored__ := $(shell ./setup.sh)

PACKAGES=syndicate syndicate-examples syndicate-msd
COLLECTS=syndicate syndicate-examples

all: setup

clean:
	find . -name compiled -type d | xargs rm -rf
	find . -name '*.rkte' | xargs rm -rf

setup:
	raco setup --check-pkg-deps --unused-pkg-deps $(COLLECTS)

link:
	raco pkg install --link $(PACKAGES)

unlink:
	raco pkg remove $(PACKAGES)

test: setup testonly

testonly:
	raco test -p $(PACKAGES)

PROTOCOLS_BRANCH=main
pull-protocols:
	git subtree pull -P syndicate/private/protocols \
		-m 'Merge latest changes from the syndicate-protocols repository' \
		git@git.syndicate-lang.org:syndicate-lang/syndicate-protocols \
		$(PROTOCOLS_BRANCH)

fixcopyright:
	-fixcopyright.rkt --preset-racket LGPL-3.0-or-later
